/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author pasinee
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer:cs.getCustomers()) {
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0888888888"));
        
        System.out.println("----Add----");
        Customer cus1 = new Customer("kob","0999999999");
        cs.addNew(cus1);
        for(Customer customer:cs.getCustomers()) {
            System.out.println(customer);
        }
        
        System.out.println("----Update----");
        Customer delCus = cs.getByTel("0999999999");
        delCus.setTel("0999999911");
        cs.update(delCus);
        for(Customer customer:cs.getCustomers()) {
            System.out.println(customer);
        }
        
        System.out.println("----Delete----");
        cs.delete(delCus);
        for(Customer customer:cs.getCustomers()) {
            System.out.println(customer);
        }
    }
}
